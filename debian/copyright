Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libpod
Source: https://github.com/containers/libpod
Comment:
 Upstream "github.com/uber/*" libraries depend on forked prometheus so
 using bundled/vendored ones make sense to avoid burden of non-reusable
 packages with heavier dependencies than vendored subset of those libs.
 .
 Protobuf in debian is not ready yet, cf. #961814
Files-Excluded:
    vendor
    test/tools/vendor
Files-Included:
    vendor/github.com/checkpoint-restore/checkpointctl
    vendor/github.com/container-orchestrated-devices
    vendor/github.com/containers/conmon
    vendor/github.com/coreos/stream-metadata-go
    vendor/github.com/digitalocean
    vendor/github.com/docker/go-plugins-helpers
    vendor/github.com/moby/sys
    vendor/github.com/uber/jaeger-client-go

Files: *
Copyright:
    2016-2019 Red Hat, Inc.
License: Apache-2.0

Files: contrib/snapcraft/*
Copyright:
    2017 Snapcrafters
 2017, Snapcrafters
License: Expat

Files: contrib/snapcraft/README.md
Copyright:
    2017 Snapcrafters
License: Expat

Files: debian/*
Copyright:
    2018-2019 Dmitry Smirnov <onlyjob@debian.org>
License: Apache-2.0

Files: docs/source/markdown/podman-image-inspect.1.md
Copyright:
    2016-2019 Red Hat, Inc.
License: Expat

Files: pkg/k8s.io/*
Copyright: belongs to the Kubernetes Authors and is licensed under Apache-2.0, also check the license headers in the files.
License: Apache-2.0

Files: pkg/k8s.io/api/*
Copyright: 2014-2017, 2020, The Kubernetes Authors.
License: Apache-2.0

Files: pkg/k8s.io/apimachinery/*
Copyright: 2014-2017, 2020, The Kubernetes Authors.
License: Apache-2.0

Files: pkg/kubeutils/*
Copyright: 2014-2017, 2020, The Kubernetes Authors.
License: Apache-2.0

Files: pkg/signal/signal_linux.go
  pkg/signal/signal_linux_mipsx.go
Copyright: 2013-2018, Docker, Inc.
License: Apache-2.0

Files: pkg/util/camelcase/*
Copyright: 2015, Fatih Arslan
License: Expat

Files: vendor/github.com/container-orchestrated-devices/container-device-interface/pkg/*
Copyright: 2021, 2022, The CDI Authors
License: Apache-2.0

Files: vendor/github.com/container-orchestrated-devices/container-device-interface/pkg/cdi/doc.go
Copyright:
    2016-2019 Red Hat, Inc.
License: Apache-2.0

Files: vendor/github.com/digitalocean/go-libvirt/const.gen.go
  vendor/github.com/digitalocean/go-libvirt/doc.go
  vendor/github.com/digitalocean/go-libvirt/libvirt.go
  vendor/github.com/digitalocean/go-libvirt/qemu_protocol.gen.go
  vendor/github.com/digitalocean/go-libvirt/remote_protocol.gen.go
  vendor/github.com/digitalocean/go-libvirt/rpc.go
  vendor/github.com/digitalocean/go-libvirt/units.go
Copyright: 2016, 2018, 2020, The go-libvirt Authors.
License: Apache-2.0

Files: vendor/github.com/digitalocean/go-libvirt/internal/*
Copyright: 2016, 2018, 2020, The go-libvirt Authors.
License: Apache-2.0

Files: vendor/github.com/digitalocean/go-libvirt/internal/go-xdr/*
Copyright: 2012-2014, Dave Collins <dave@davec.name>
License: ISC

Files: vendor/github.com/digitalocean/go-qemu/qmp/qmp.go
  vendor/github.com/digitalocean/go-qemu/qmp/rpc.go
  vendor/github.com/digitalocean/go-qemu/qmp/socket.go
  vendor/github.com/digitalocean/go-qemu/qmp/socket_unix.go
  vendor/github.com/digitalocean/go-qemu/qmp/socket_windows.go
Copyright: 2016, The go-qemu Authors.
License: Apache-2.0

Files: vendor/github.com/docker/go-plugins-helpers/NOTICE
Copyright: 2012-2015, Docker, Inc.
License: Expat

Files: vendor/github.com/gorilla/*
Copyright: 2012, Rodrigo Moraes
License: BSD-3-clause

Files: vendor/github.com/gorilla/schema/*
Copyright: 2012, Rodrigo Moraes
License: BSD-3-clause

Files: vendor/github.com/uber/*
Copyright: 2017,2018 Uber Technologies, Inc.
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: BSD-3-clause
 This software is Copyright (c) 2020 by X. Ample.
 .
 This is free software, licensed under:
 .
   The (three-clause) BSD License
 .
 The BSD License
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 .
   * Neither the name of X. Ample nor the names of its
     contributors may be used to endorse or promote products derived from
     this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
